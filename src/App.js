import React, { useState } from "react";
import image from "./assets/img/cita.png";
import "./assets/css/App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import BarChart from "./components/BarChart";

const App = () => {
  const medicalAppointments = [];

  const [data, setData] = useState(medicalAppointments);
  const [newData, setNewData] = useState({
    name: "",
    costAppointment: "",
    date: "",
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setNewData((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const insertar = () => {
    var valueToInsert = newData;
    data.push(valueToInsert);
    setNewData(data);
  };

  return (
    <div className="container-fluid p-2">
      <div className="row mt-4 mb-4 justify-content-center">
        <div className="col-md-10">
          <h1 className="text-center">Appointments and revenue per month</h1>
        </div>
      </div>
      <div className="row mt-4 mb-4 justify-content-center">
        <div className="col-md-10">
          <BarChart />
        </div>
      </div>
      <br />
      <hr className="mb-4 mt-4" />
      <br />
      <div className="p-4 row justify-content-center">
        <div className="col-md-5 d-flex flex-column justify-content-center align-items-center">
          <div className="row mb-4">
            <h3 className="text-center">Agregar registro de cita médica</h3>
          </div>
          <div className="form-container">
            <div className="card p-2-5 mb-4 wow fadeIn">
              <div className="row">
                <div className="col-md-12">
                  <div className="row mb-2 justify-content-center">
                    <img
                      src={image}
                      alt="Agregar Cita"
                      title="Agregar Cita"
                      className="img-login mb-4"
                    />
                  </div>
                  <div className="row mb-2">
                    <div className="col-md-12">
                      <div className="form-group">
                        <label htmlFor="nombre">Nombre de paciente:</label>
                        <input
                          type="text"
                          className="form-control"
                          id="nombre"
                          name="name"
                          value={newData ? newData.name : ""}
                          onChange={handleChange}
                          autoFocus
                        />
                        <small
                          className="text-danger p-absolute mt-1"
                          id="error-usuario"
                        ></small>
                      </div>
                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-md-12">
                      <div className="form-group">
                        <label htmlFor="valor">Valor de consulta:</label>
                        <input
                          type="text"
                          className="form-control"
                          name="costAppointment"
                          id="valor"
                          value={newData ? newData.costAppointment : ""}
                          onChange={handleChange}
                        />
                        <small
                          className="text-danger p-absolute mt-1"
                          id="error-usuario"
                        ></small>
                      </div>
                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-md-12">
                      <div className="form-group">
                        <label htmlFor="fecha">Fecha de cita:</label>
                        <input
                          type="date"
                          className="form-control"
                          id="fecha"
                          name="date"
                          value={newData ? newData.date : ""}
                          onChange={handleChange}
                        />
                        <small
                          className="text-danger p-absolute mt-1"
                          id="error-usuario"
                        ></small>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-12">
                      <div className="form-group d-flex justify-content-center">
                        <button
                          type="button"
                          className="btn
                          btn-primary radius"
                          onClick={insertar}
                        >
                          AGREGAR
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-md-5 d-flex flex-column align-items-center">
          <div className="row mb-4">
            <h3 className="text-center">Citas médicas registradas</h3>
          </div>
          <div className="row">
            <div className="table-responsive">
              <table className="table table-striped table-hover text-center">
                <thead>
                  <tr>
                    <th>Nombre paciente</th>
                    <th>Valor de cita ($)</th>
                    <th>Fecha</th>
                  </tr>
                </thead>
                <tbody>
                  {data.map((element, i) => (
                    <tr key={i}>
                      <td>{element.name}</td>
                      <td>{element.costAppointment}</td>
                      <td>{element.date}</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default App;
