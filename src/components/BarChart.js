import React, { Component } from "react";
import { Bar } from "react-chartjs-2";

const data = [
  {
    year: 2019,
    month: 12,
    appointments: 145,
    revenue: 130,
  },
  {
    year: 2020,
    month: 1,
    appointments: 150,
    revenue: 164.13,
  },
  {
    year: 2020,
    month: 2,
    appointments: 98,
    revenue: 114.0,
  },
  {
    year: 2020,
    month: 3,
    appointments: 130,
    revenue: 144.04,
  },
  {
    year: 2020,
    month: 4,
    appointments: 100,
    revenue: 124.04,
  },
];

function MonthName(numberMonth) {
  let monthName = "";
  switch (numberMonth) {
    case 1:
      monthName = "January";
      break;
    case 2:
      monthName = "February";
      break;
    case 3:
      monthName = "March";
      break;
    case 4:
      monthName = "April";
      break;
    case 5:
      monthName = "May";
      break;
    case 6:
      monthName = "June";
      break;
    case 7:
      monthName = "July";
      break;
    case 8:
      monthName = "August";
      break;
    case 9:
      monthName = "September";
      break;
    case 10:
      monthName = "October";
      break;
    case 11:
      monthName = "November";
      break;
    case 12:
      monthName = "December";
      break;
  }

  return monthName;
}

function createLabels() {
  let labels = [];
  for (let i = 0; i < data.length; i++) {
    labels.push(MonthName(data[i].month) + ", " + data[i].year);
  }

  return labels;
}

function createData(dataset, value) {
  let data = [];
  for (let i = 0; i < dataset.length; i++) {
    data.push(dataset[i][value]);
  }

  return data;
}

const formatter = new Intl.NumberFormat("en-US", {
  style: "currency",
  currency: "USD",
  minimumFractionDigits: 0,
});

const BarChart = () => {
  return (
    <div>
      <Bar
        data={{
          labels: createLabels(),
          datasets: [
            {
              label: "Appointments",
              backgroundColor: "#36b9cc",
              hoverBackgroundColor: "rgba(54,185,204,0.7)",
              borderColor: "#4e73df",
              data: createData(data, "appointments"),
            },
            {
              label: "Revenue",
              backgroundColor: "#00BBF9",
              hoverBackgroundColor: "#63D2FF",
              borderColor: "#63D2FF",
              data: createData(data, "revenue"),
            },
          ],
        }}
        height={400}
        width={600}
        options={{
          maintainAspectRatio: false,
          layout: {
            padding: {
              left: 10,
              right: 25,
              top: 25,
              bottom: 0,
            },
          },
          scales: {
            xAxes: [
              {
                time: {
                  unit: "month",
                },
                gridLines: {
                  display: false,
                  drawBorder: false,
                },
                ticks: {
                  maxTicksLimit: 12,
                },
                maxBarThickness: 25,
              },
            ],
            yAxes: [
              {
                ticks: {
                  min: 0,
                  max: 200,
                  maxTicksLimit: 8,
                  padding: 10,
                  // Include a dollar sign in the ticks
                  callback: function (value, index, values) {
                    return formatter.format(value);
                  },
                },
                gridLines: {
                  color: "rgb(234, 236, 244)",
                  zeroLineColor: "rgb(234, 236, 244)",
                  drawBorder: false,
                  borderDash: [2],
                  zeroLineBorderDash: [2],
                },
              },
            ],
          },
          legend: {
            display: false,
          },
          tooltips: {
            titleMarginBottom: 10,
            titleFontColor: "#6e707e",
            titleFontSize: 14,
            backgroundColor: "rgb(255,255,255)",
            bodyFontColor: "#858796",
            borderColor: "#dddfeb",
            borderWidth: 1,
            xPadding: 15,
            yPadding: 15,
            displayColors: false,
            caretPadding: 10,
            callbacks: {
              label: function (tooltipItem, chart) {
                var datasetLabel =
                  chart.datasets[tooltipItem.datasetIndex].label || "";
                return (
                  datasetLabel + ": " + formatter.format(tooltipItem.yLabel)
                );
              },
            },
          },
        }}
      />
    </div>
  );
};

export default BarChart;
